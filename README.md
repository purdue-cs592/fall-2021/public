<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>
 
# CS 592: Programmable Data Planes for Self-Driving Networks

> **Eligible for both M.S. and Ph.D. plan-of-study at Purdue!**

[[_TOC_]] 

## Logistics
- Instructor: [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- Lecture time: TTh 12:00-1:15pm
- Location: [LWSN B134](https://goo.gl/maps/5e4yKBQ897czMyw8A)
- Credit Hours: 3.00
- Course discussion and announcements: [Campuswire](https://campuswire.com/c/GA2BF755D)
- Paper reviews: [Perusall](https://app.perusall.com/courses/purdue-cs592-pdp/_/dashboard)
- Project proposal, report, and link to a YouTube video (extra credit): [HotCRP](https://purdue-cs592-fall21.hotcrp.com/)
- Office hours
  - Thursdays 2:00-3:00pm, [LWSN 1123-F](https://goo.gl/maps/5e4yKBQ897czMyw8A), Muhammad Shahbaz

> **Note:** Visit [Brightspace](https://purdue.brightspace.com/d2l/home/373996) for instructions on joining [Campuswire](https://campuswire.com/c/GA2BF755D), [Gradescope](https://www.gradescope.com/courses/285764), [Perusall](https://app.perusall.com/courses/purdue-cs592-pdp/_/dashboard), and [HotCRP](https://purdue-cs592-fall21.hotcrp.com/).

## Course Description

Emerging trends such as cloud computing, the internet of things, and augmented and virtual reality demand highly responsive, available, secure, and scalable networks to meet users' quality of experience expectations.
Operators currently manage these networks and protocols using a variety of ad-hoc tools and scripts; however, the unpredictable and complex interactions between network conditions and workloads make such manual tuning difficult. 

Machine learning (ML) can help approximate and automate these complex interactions that govern today's hyper-scale datacenter networks (e.g., Google, Microsoft, Amazon, and Facebook). 
Recent proposals generate ML models for networks to produce recommendations for policies like routing and congestion control.
At present, these models run on a logically-centralized control plane that infers learned polices, causing delays of tens of milliseconds when updating network devices.
Therefore, for policies like anomaly detection, where inputs to the ML model may vary over time (e.g., payload size or time-windowed features), most packets---even of a single flow---need to traverse the control plane; thus, significantly increasing load on the controller and inflating flow latencies.

In this new graduate-level seminar course, we will (a) learn recent advances in networking, like software-defined networks (SDN) and programmable networks, (b) study how these advances, in combination with machine learning, are enabling novel and automous ways of managing today's mega-scale networks, and, finally, (c) explore the various challenges that still exist in realizing the true vision of self-driving networks, which will operate autonomously and securely with high throughput and low latency.

The goals for this course are:

- To become familiar with the recent advances in computer networks, like software-defined networks (SDN) and programmable networks.
- To learn what's the state-of-the-art in machine learning for networks and vice versa: **ML for Networks and Networks for ML**.
- To gain practice in reading research papers, published at top networking, machine learning, and architecture conferences, and to critically understand others' research.
- To gain experience with network programming using state-of-the-art programmable research platforms (P4, ONOS, Stratum OS, Barefoot Tofino, Spatial, and Xilinx SDNet).
- To apply this experience in implementing novel ML hardware designs and applications for networking.

## Prerequisites and Background

The course does not require any offifical prerequisites; but expect students to have some basic familiarity with computer networks, machine learning (ML), and distributed systems. 
Students without such basic understanding should discuss with the instructor prior to registering for the course.

## Recommended Textbooks
- Computer Networks: A Systems Approach by L. Peterson and B. Davie ([Online Version](https://book.systemsapproach.org/))
- Software-Defined Networks: A Systems Approach - The New Network Stack ([Online Version](https://sdn.systemsapproach.org/)) 
- Network Algorithmics: An Interdisciplinary Approach to Designing Fast Networked Devices (1st Edition), available online @ [Purdue Library](https://purdue-primo-prod.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=TN_cdi_safari_books_9780120884773&context=PC&vid=PURDUE&lang=en_US&search_scope=everything&adaptor=primo_central_multiple_fe&tab=default_tab&query=any,contains,network%20algorithmics&offset=0)

> Other optional but interesting resources: [Sytems Approach - Blog](https://www.systemsapproach.org/blog).

## Course Schedule

> **Note:** 
> This syllabus and schedule is preliminary and subject to change.

| Date    | Topics  | Presenter | Notes | Optional Readings & Exercises |
| :------ | :------ | :-------- | :---- | :---------------------------- |
| **Week 1** | **Course Overview** | | | |
| Tue <br> Aug 24 | *[SIGCOMM Conference](https://conferences.sigcomm.org/sigcomm/2021/program.html):* No class | | |
| Thu <br> Aug 26 | Introduction | [Muhammad Shahbaz](https://cs.purdue.edu/~mshahbaz/) | | [How to Read a Paper](readings/HowToRead2017.pdf) |
| **Week 2** | **Self-Driving Networks I** | | | |
| Tue <br> Aug 31 | [Why (and How) Networks Should Run Themselves](https://arxiv.org/pdf/1710.11583.pdf) (ANRW '18) | [Samuel Youssef](https://www.cs.purdue.edu/people/graduate-students/syousse.html) | | |
| Thu <br> Sep 02 | [A Knowledge Plane for the Internet](https://dl.acm.org/doi/10.1145/863955.863957) (SIGCOMM '03) | [Annus Zulfiqar](https://annusgit.github.io/) | | |
| **Week 3** | **Software-Defined Networks I** | | | |
| Tue <br> Sep 07 | [OpenFlow: Enabling Innovation in Campus Networks](https://dl.acm.org/doi/10.1145/1355734.1355746) (SIGCOMM CCR '08) | [Eman Diyab](https://www.cs.purdue.edu/people/graduate-students/ediyab.html) | | [How to Review a Paper](https://greatresearch.org/2013/10/18/the-paper-reviewing-process/) |
| Thu <br> Sep 09 | [SDX: A Software Defined Internet Exchange](https://dl.acm.org/doi/10.1145/2740070.2626300) (SIGCOMM '14) | [Ertza Warraich](https://ertza.me/) | | Practice [Virtual Networks using Mininet, Stratum, and ONOS](https://gitlab.com/purdue-cs536/spring-2021/public/-/tree/master/assignments/assignment0) |
| **Week 4** | **Programmable Networks and Data Planes I** | | | |
| Tue <br> Sep 14 | [Forwarding Metamorphosis: Fast Programmable Match-Action Processing in Hardware for SDN](https://dl.acm.org/doi/10.1145/2486001.2486011) (SIGCOMM '13) | [Juexiao Wang](https://www.linkedin.com/in/juexiaowang/) | | [Knowing the unknowns in the academic publishing world for incoming Ph.D. students](https://gitlab.com/mshahbaz/mshahbaz.gitlab.io/-/wikis/Knowing-the-unknowns-in-the-academic-publishing-world-for-incoming-Ph.D.-students) |
| Thu <br> Sep 16 | [Programmable Packet Scheduling at Line Rate](https://dl.acm.org/doi/10.1145/2934872.2934899) (SIGCOMM '16) | [Jonathan Rosenthal](https://www.cs.purdue.edu/people/graduate-students/rosenth0.html) | | |
| **Week 5** | **ML for Networks I** | | | |
| Tue <br> Sep 21 | [Learning in Situ: A Randomized Experiment in Video Streaming](https://www.usenix.org/conference/nsdi20/presentation/yan) (NSDI '20) | Zixuan Li | | |
| Thu <br> Sep 23 | [Learning Scheduling Algorithms for Data Processing Clusters](https://dl.acm.org/doi/10.1145/3341302.3342080) (SIGCOMM '19) | [Umakant Kulkarni](https://www.cs.purdue.edu/people/graduate-students/ukulkarn.html) | | |
| **Week 6** | **Networks for ML I** | | | |
| Tue <br> Sep 28 | [In-Network Aggregation for Shared Machine Learning Clusters](https://people.csail.mit.edu/ghobadi/papers/panama.pdf) (MLSys '21) | [Leo Liu](https://www.linkedin.com/in/leonard-yuming-liu/) | | |
| Thu <br> Sep 30 | [Accelerating Distributed Reinforcement Learning with In-Switch Computing](https://dl.acm.org/doi/10.1145/3307650.3322259) (ISCA '19) | [Jingqi Huang](https://jingqihuang.github.io/) | | |
| **Week 7** | **ML Hardware Architectures I** | | | |
| Tue <br> Oct 05 | No class | | |
| Thu <br> Oct 07 | [Plasticine: A Reconfigurable Architecture For Parallel Paterns](https://dl.acm.org/doi/10.1145/3140659.3080256) (ISCA '17) | [Muhammad Shahbaz](https://cs.purdue.edu/~mshahbaz/) | | |
| **Week 8** | **ML Hardware Architectures I (contd.)**  | | | |
| Tue <br> Oct 12 | *October Break*: No Class | | | |
| Thu <br> Oct 14 | [A Domain-Specific Architecture for Deep Neural Networks](https://dl.acm.org/doi/pdf/10.1145/3154484) (CACM '18) | [Annus Zulfiqar](https://annusgit.github.io/) | | |
| **Week 8** | **ML Network Data Planes I**  | | | |
| Tue <br> Oct 19 | [Taurus: An Intelligent Data Plane](https://arxiv.org/pdf/2002.08987.pdf) (Arxiv '20) | [Ertza Warraich](https://ertza.me/) | | |
| Thu <br> Oct 21 | [Do Switches Dream of Machine Learning?: Toward In-Network Classification](https://dl.acm.org/doi/10.1145/3365609.3365864) (HotNets '19) | [Juexiao Wang](https://www.linkedin.com/in/juexiaowang/) | | |
| **Week 9** | **Self-Driving Networks II** | | | |
| Tue <br> Oct 26 | No Class | | | [NSF Workshop on Measurements for Self-Driving Networks](https://sites.cs.ucsb.edu/~arpitgupta/pdfs/measure_selfdn_workshop.pdf) (NSF Report '19) |
| Thu <br> Oct 28 | Course Paper Progress and Updates | All | | |
| **Week 10** | **Software-Defined Networks II** | | | |
| Tue <br> Nov 02 | [Architecting for Innovation](https://dl.acm.org/doi/10.1145/2002250.2002256) (CCR '11) | [Jonathan Rosenthal](https://www.cs.purdue.edu/people/graduate-students/rosenth0.html) | | |
| Thu <br> Nov 04 | *Final Exam* | | | |
| **Week 11** | **Programmable Networks and Data Planes II** | | | |
| Tue <br> Nov 09 | [The Design and Implementation of Open vSwitch](https://www.usenix.org/conference/nsdi15/technical-sessions/presentation/pfaff) (NSDI '15) | Zixuan Li | | |
| Thu <br> Nov 11 | Course Paper Progress and Updates | All | | |
| **Week 12** | **ML for Networks II** | | | |
| Tue <br> Nov 16 | [Is Advance Knowledge of Flow Sizes a Plausible Assumption?](https://www.usenix.org/system/files/nsdi19-dukic.pdf) (NSDI '19) | [Leo Liu](https://www.linkedin.com/in/leonard-yuming-liu/) | | |
| Thu <br> Nov 18 | Course Paper Progress and Updates | All | | |
| **Week 13** | **Networks for ML II** | | | |
| Tue <br> Nov 23 | [Scaling Distributed Machine Learning with In-Network Aggregation](https://www.usenix.org/system/files/nsdi21-sapio.pdf) (NSDI '21) | [Umakant Kulkarni](https://www.cs.purdue.edu/people/graduate-students/ukulkarn.html) | | |
| Thu <br> Nov 25 | *Thanksgiving Vacation*: No Class | | | |
| **Week 14** | **ML Hardware Architectures II** | | | |
| Tue <br> Nov 30 | [Spatial: A Language and Compiler for Application Accelerators](https://dl.acm.org/doi/10.1145/3192366.3192379) (PLDI '18) | [Jingqi Huang](https://jingqihuang.github.io/) | | [The Next Wave in Cloud Systems Architecture](https://www.computer.org/csdl/magazine/co/2021/10/09548131/1x9TJR9OYow) |
| Thu <br> Dec 02 | Course Paper Progress and Updates | All | | |
| **Week 15** | **ML Network Data Planes II**  | | | |
| Tue <br> Dec 07 | [FPGA Research Design Platform Fuels Network Advances](http://yuba.stanford.edu/~nickm/papers/Xcell.pdf) (Xcell '10) | [Eman Diyab](https://www.cs.purdue.edu/people/graduate-students/ediyab.html) / [Muhammad Shahbaz](https://cs.purdue.edu/~mshahbaz/) | | |
| Thu <br> Dec 09 | Course Paper Progress and Updates | All | | |
| **Week 15** | **Exam Week**  | | | |
| Tue <br> Dec 14 | *Course Paper Presentations* | All | | |
| Thu <br> Dec 16 | *Course Paper Presentations* | All | | |

## Paper Reading and Discussion
In this course, we will be reading and discussing recent (as well as some classical and seminal) papers from top networking (e.g., ACM SIGCOMM and USENIX), machine learning (e.g., NeurIPS and ICML), systems (e.g., USENIX OSDI and SOSP) and architecture (e.g., ACM/IEEE ISCA and IEEE ASPLOS) conferences. 
Students can also propose to discuss papers of their choice as long as the proposed papers fit within the scope of the course.

- **Self-Driving Networks (SelfDN)**
  - [Why (and How) Networks Should Run Themselves](https://dl.acm.org/doi/10.1145/3232755.3234555)
  - [Self-Programming Networks: Architecture and Algorithms](https://ieeexplore.ieee.org/document/8262813)
  - [A Knowledge Plane for the Internet](https://dl.acm.org/doi/10.1145/863955.863957)
  - [Knowledge-Defined Networking](https://dl.acm.org/doi/10.1145/3138808.3138810)
  - [Workshop on Self-Driving Networks — Report](https://www.cs.princeton.edu/~jrex/papers/self-driving-networks18.pdf)
  - [NSF Workshop on Measurements for Self-Driving Networks](https://sites.cs.ucsb.edu/~arpitgupta/pdfs/measure_selfdn_workshop.pdf) 
- **Software-Defined Networks (SDN)**
  - [The Road to SDN: An Intellectual History of Programmable Networks](https://dl.acm.org/doi/abs/10.1145/2602204.2602219)
  - [OpenFlow: Enabling Innovation in Campus Networks](https://dl.acm.org/doi/10.1145/1355734.1355746)
  - [Architecting for Innovation](https://dl.acm.org/doi/10.1145/2002250.2002256)
  - [Reproducible Network Experiments using Container-based Emulation](https://dl.acm.org/doi/10.1145/2413176.2413206)
  - [SDX: A Software Defined Internet Exchange](https://dl.acm.org/doi/10.1145/2740070.2626300)
  - [Elmo: Source Routed Multicast for Public Clouds](https://dl.acm.org/doi/10.1145/3341302.3342066)
  - [ONOS: Towards an Open, Distributed SDN OS](https://dl.acm.org/doi/10.1145/2620728.2620744)
- **Programmable Networks and Data Planes**
  - [P4: Programming Protocol-Independent Packet Processors](https://dl.acm.org/doi/10.1145/2656877.2656890)
  - [Using Deep Programmability to Put Network Owners in Control](https://dl.acm.org/doi/10.1145/3431832.3431842)
  - [Forwarding metamorphosis: fast programmable match-action processing in hardware for SDN](https://dl.acm.org/doi/10.1145/2486001.2486011)
  - [dRMT: Disaggregated Programmable Switching](https://dl.acm.org/doi/10.1145/3098822.3098823)
  - [Programmable Packet Scheduling at Line Rate](https://dl.acm.org/doi/10.1145/2934872.2934899)
  - [The Design and Implementation of Open vSwitch](https://www.usenix.org/conference/nsdi15/technical-sessions/presentation/pfaff)
  - [PISCES: A Programmable, Protocol-Independent Software Switch](https://dl.acm.org/doi/10.1145/2934872.2934886)
  - [Compiling Packet Programs to Reconfigurable Switches](https://www.usenix.org/conference/nsdi15/technical-sessions/presentation/jose)
  - [Design Principles for Packet Parsers](https://dl.acm.org/doi/10.5555/2537857.2537860)
- **ML for Networks**
  - [Elastic RSS: Co-Scheduling Packets and Cores Using Programmable NICs](https://dl.acm.org/doi/10.1145/3343180.3343184)
  - [Neural Packet Classification](https://dl.acm.org/doi/10.1145/3341302.3342221)
  - [Neural Packet Routing](https://dl.acm.org/doi/10.1145/3405671.3405813)
  - [Learning To Route](https://conferences.sigcomm.org/hotnets/2017/papers/hotnets17-final28.pdf)
  - [A Deep Reinforcement Learning Perspective on Internet Congestion Control](http://proceedings.mlr.press/v97/jay19a/jay19a.pdf)
  - [Neural Adaptive Video Streaming with Pensieve](https://dl.acm.org/doi/10.1145/3098822.3098843)
  - [Learning in situ: a randomized experiment in video streaming](https://www.usenix.org/conference/nsdi20/presentation/yan)
  - [Placeto: Learning Generalizable Device Placement Algorithms for Distributed Machine Learning](https://people.csail.mit.edu/hongzi/content/publications/placeto-neurips19.pdf)
  - [Learning Scheduling Algorithms for Data Processing Clusters](https://dl.acm.org/doi/10.1145/3341302.3342080)
  - [Is Advance Knowledge of Flow Sizes a Plausible Assumption?](https://www.usenix.org/system/files/nsdi19-dukic.pdf)
- **Networks for ML**
  - [Can the Network be the AI Accelerator?](https://dl.acm.org/doi/abs/10.1145/3229591.3229594)
  - [In-network Aggregation for Shared Machine Learning Clusters](https://people.csail.mit.edu/ghobadi/papers/panama.pdf)
  - [Scaling Distributed Machine Learning with In-Network Aggregation](https://www.microsoft.com/en-us/research/uploads/prod/2019/04/switchml-tr19.pdf)
  - [Accelerating Distributed Reinforcement Learning with In-Switch Computing](https://dl.acm.org/doi/10.1145/3307650.3322259)
- **ML Hardware Architectures**
  - [Plasticine: A Reconfigurable Architecture For Parallel Paterns](https://dl.acm.org/doi/10.1145/3140659.3080256)
  - [Spatial: A Language and Compiler for Application Accelerators](https://dl.acm.org/doi/10.1145/3192366.3192379)
  - [A Domain-Specific Architecture for Deep Neural Networks](https://dl.acm.org/doi/10.1145/3154484)
  - [TensorFlow: A System for Large-Scale Machine Learning](https://www.usenix.org/conference/osdi16/technical-sessions/presentation/abadi)
  - [PyTorch: An Imperative Style, High-Performance Deep Learning Library](https://arxiv.org/pdf/1912.01703.pdf)
  - [A Configurable Cloud-Scale DNN Processor for Real-Time AI](https://www.microsoft.com/en-us/research/uploads/prod/2018/06/ISCA18-Brainwave-CameraReady.pdf)
- **ML Network Data Planes**
  - [FPGA Research Design Platform Fuels Network Advances](http://yuba.stanford.edu/~nickm/papers/Xcell.pdf)
  - [Taurus: An Intelligent Data Plane](https://arxiv.org/pdf/2002.08987.pdf)
  - [Do Switches Dream of Machine Learning?: Toward In-Network Classification](https://dl.acm.org/doi/10.1145/3365609.3365864)
  - [Running Neural Networks on the NIC](https://arxiv.org/abs/2009.02353)
  - [In-network Neural Networks](https://arxiv.org/pdf/1801.05731.pdf)

You should read each paper and add comments and questions along with a 1-page summary of the paper on [Perusall](https://app.perusall.com/courses/purdue-cs592-pdp/_/dashboard) by the due date below. Please plan to provide at least five comments or questions for each paper on Perusall ahead of the associated class and follow the comments from other students and the course staff. Please come to class prepared with several points that will substantially contribute to the group discussion. 

> **Note:** General tips on reading papers are [here](readings/HowToRead2017.pdf). 

Grades for your class participation and paper reviews will be determined based on attendance and, more importantly, substantial contributions to paper discussions both on Perusall and in class. 

> **Note:** What we expect you to know and prepare before each discussion is [here](reading-papers.md).

## Course Exam
There will be one final exam based on course content (i.e., paper readings and presenations).

- Final exam `11/04/2021`

## Course Project

The course includes a semester-long systems-building project that should be done in groups (either of size two or three, to be determined by the instructor after the course size is finalized) and must involve some programming. All group students are expected to share equally in the implementation. Students are expected to implement novel research projects, closely related to the material and topics covered in the course.

> **Note:** If you are uncertain about the project's satisfiability, please contact the instructor as soon as possible.

### Project proposal
For the course project, students should include the following information.

- **Background**
  - What problem is research attempting to solve?
  - Why is the problem important?
  - How does this relate directly to topics or papers covered in class
- **Novelty**
  - What is the current state-of-the-art in related work, and why are they insufficient?
  - What is your (novel) technical insight/approach to solving this problem
- **Plan**
  - Proposed implementation (language, framework, etc.)
  - Key questions that evaluation will address
  - Evaluation strategy (testing platform/setup, simulated data/traces, etc.)
  - What does "success" look like? How do you quantify/compare results to alternative approaches / related work?

Proposals should be submitted via [HotCRP](https://purdue-cs592-fall21.hotcrp.com/).

> **Note:** If students are concerned that their proposed project might not be sufficiently relevant to the course to satisfy the topical criteria, please contact instructors earlier than later. Proposals not closely related to the topical matter may be rejected outright as inappropriate.

### Project selection
The project proposal from above will be finalized. This may involve some back-and-forth between instructors and the group (likely via [Campuswire](https://campuswire.com/c/GA2BF755D)).

### Final report
The final report will be in the form of a paper written in Latex and submitted as a PDF file via [HotCRP](https://purdue-cs592-fall21.hotcrp.com/).

> **Note:** 
> The project writeup should be six pages of double-column, single-spaced, 10-point font (excluding references, which can go on extra pages), similar in spirit to a workshop paper. Papers must be typeset in LaTeX using the [ACM `sigconf` template](https://www.overleaf.com/latex/templates/acm-conference-proceedings-primary-article-template/wbvnghjbzwpc).

### Deliverables

- Project proposal `09/23/2021`
- Project report (final) `12/18/2021`
- Project presentation (slides) `12/18/2021`
- YouTube video/demo (extra credit) `12/18/2021`

## Grading

- Class participation and presentations: 20%
- Research paper questions and summaries: 10%
- Project proposal: 10%
- Project presentation: 15%
- Project report (final): 20% (and 5% extra credit for video/demo posted on YouTube.)
- Final exam: 25%

## Policies

### Late submission

- Grace period: 24 hours for the entire semester.
- After the grace period, 25% off for every 24 hours late, rounded up.

If you have extenuating circumstances that result in an assignment being late, please let us know about them as soon as possible.

### Academic integrity

We will default to Purdue's academic policies throughout this course unless stated otherwise. You are responsible for reading the pages linked below and will be held accountable for their contents.
- http://spaf.cerias.purdue.edu/integrity.html
- http://spaf.cerias.purdue.edu/cpolicy.html

### Honor code

By taking this course, you agree to take the [Purdue Honors Pledge](https://www.purdue.edu/odos/osrr/honor-pledge/about.html): "As a boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together - we are Purdue."

### COVID-19 and quarantine + isolation!

Please visit [Protect Purdue Plan](https://protect.purdue.edu/plan/) or [Fall 2021 resources](https://www.purdue.edu/innovativelearning/teaching-remotely/resources.aspx) for most up-to-date guidelines and instructions.
